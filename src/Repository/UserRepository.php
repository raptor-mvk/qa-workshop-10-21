<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository
{
    /**
     * @return User[]
     */
    public function getUsers(int $page, int $perPage): array
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('t')
            ->from($this->getClassName(), 't')
            ->orderBy('t.id', 'DESC')
            ->setFirstResult($perPage * $page)
            ->setMaxResults($perPage);

        return $qb->getQuery()->getResult();
    }

    public function getLastCounter(): int
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select($qb->expr()->max('u.counter'))
            ->from($this->getClassName(), 'u');

        return $qb->getQuery()->getSingleScalarResult() ?? 0;
    }
}
