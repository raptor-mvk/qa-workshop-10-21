<?php

namespace App\Controller\Api\v1;

use App\Entity\User;
use App\Event\CreateUserEvent;
use App\Exception\DeprecatedApiException;
use App\Manager\UserManager;
use Psr\EventDispatcher\EventDispatcherInterface;
use App\DTO\SaveUserDTO;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;
use OpenApi\Annotations as OA;

/**
 * @Route("/api/v1/user")
 */
class UserController extends AbstractController
{
    private UserManager $userManager;

    private EventDispatcherInterface $eventDispatcher;

    private Environment $twig;

    public function __construct(UserManager $userManager, EventDispatcherInterface $eventDispatcher, Environment $twig)
    {
        $this->userManager = $userManager;
        $this->eventDispatcher = $eventDispatcher;
        $this->twig = $twig;
    }

    /**
     * @Route("", methods={"GET"})
     */
    public function getUsersAction(Request $request): Response
    {
        $perPage = $request->query->get('perPage');
        $page = $request->query->get('page');
        $users = $this->userManager->getUsers($page ?? 0, $perPage ?? 200);
        $code = empty($users) ? 204 : 200;

        return new JsonResponse(['users' => array_map(static fn(User $user) => $user->toArray(), $users)], $code);
    }
}
