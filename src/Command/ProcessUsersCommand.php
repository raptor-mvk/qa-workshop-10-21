<?php

namespace App\Command;

use App\DTO\SaveUserDTO;
use App\Entity\User;
use App\Manager\UserManager;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ProcessUsersCommand extends Command
{
    private const CONGRATS = [
/*        1 => 'Отлично! Ты - чемпион',
        2 => 'Очень хорошо! У тебя второе место',
        3 => 'Замечательно! У тебя третий результат',
        5 => 'Хорошая работа! Ты в топ-5',
        10 => 'Ура! Ты в первой десятке',*/
        1 => 'Поздравляю! Ты справился с заданием'
    ];

    private UserManager $userManager;

    private string $rootPath;

    public function __construct(UserManager $userManager, string $rootPath)
    {
        parent::__construct();
        $this->userManager = $userManager;
        $this->rootPath = $rootPath;
    }

    protected function configure(): void
    {
        $this->setName('users:process')
            ->setDescription('Adds users to database');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $counter = $this->userManager->getLastCounter() + 1;
        $directoryIterator = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($this->rootPath.'/users', RecursiveDirectoryIterator::SKIP_DOTS),
            RecursiveIteratorIterator::CHILD_FIRST,
        );
        foreach ($directoryIterator as $fileOrDirectory) {
            if (is_dir($fileOrDirectory)) {
                continue;
            }
            $handle = fopen($fileOrDirectory, 'rb');
            $lines = [];
            while(!feof($handle)) {
                $lines[] = trim(fgets($handle));
            }
            if (count($lines) >= 2) {
                if ($this->userManager->findUserByLogin($lines[0]) === null) {
                    $output->writeln(sprintf('New user %s found, adding to database', $lines[0]));
                    $userDTO = new SaveUserDTO([
                            'login' => $lines[0],
                            'password' => $lines[1],
                            'roles' => ['ROLE_USER'],
                            'age' => 0,
                            'isActive' => true]
                    );
                    $userId = $this->userManager->saveUserFromDTO(new User(), $userDTO);
                    $user = $this->userManager->findUserById($userId);
                    $user->setCounter($counter);
                    foreach (self::CONGRATS as $value => $text) {
                        if ($value <= $counter) {
                            $user->setCongrats($text);
                        }
                    }
                    $this->userManager->flush();
                }
            }
        }

        return 0;
    }
}
